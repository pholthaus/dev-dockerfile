#/bin/bash

# TEX IMAGE

# update system
sed -i 's/htt[p|ps]:\/\/archive.ubuntu.com\/ubuntu\//mirror:\/\/mirrors.ubuntu.com\/mirrors.txt/g' /etc/apt/sources.list
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 871920D1991BC93C
apt-get update
apt-get dist-upgrade -y

# install tools
export DEBIAN_FRONTEND="noninteractive"
export TZ="Europe/London"

apt-get install openssh-client -y
apt-get install rsync -y
apt-get install python3 -y
apt-get install python3-venv -y
apt-get install python3-pip -y
apt-get install gettext-base -y
apt-get install imagemagick -y
apt-get install qpdf -y
apt-get install gcc -y
apt-get install texlive-full -y

# configuration
sed -i '/disable ghostscript format types/,+6d' /etc/ImageMagick-6/policy.xml

# free up space
apt-get purge $(dpkg -l | cut -d ' ' -f 3 | grep -- '.*-doc$') -y
apt-get purge $(dpkg -l | cut -d ' ' -f 3 | grep -- '^texlive-lang.*' | grep -v -e german -e english -e greek) -y
apt-get clean -y

