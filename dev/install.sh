#/bin/bash

# DEV IMAGE

# update system
sed -i 's/htt[p|ps]:\/\/archive.ubuntu.com\/ubuntu\//mirror:\/\/mirrors.ubuntu.com\/mirrors.txt/g' /etc/apt/sources.list
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 871920D1991BC93C
apt-get update
apt-get dist-upgrade -y

# install tools
export DEBIAN_FRONTEND="noninteractive"
export TZ="Europe/London"

apt-get install openssh-client -y
apt-get install rsync -y
apt-get install python3 -y
apt-get install python3-venv -y
apt-get install python3-pip -y
apt-get install gettext-base -y
apt-get install gcc -y

# free up space
apt-get clean -y
